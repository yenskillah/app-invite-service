USE appInvite;

CREATE TABLE IF NOT EXISTS authentication (
    id int(11) unsigned NOT NULL AUTO_INCREMENT,
    userId int(11) DEFAULT NULL,
    token text,
    dateCreated timestamp NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY userId (userId)
  ) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS invites (
    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    userId bigint(20) NOT NULL,
    token text NOT NULL,
    clientCode varchar(12) NOT NULL DEFAULT '',
    clientName text NOT NULL,
    status enum('active','inactive','used','expired') NOT NULL DEFAULT 'active',
    dateCreated datetime NOT NULL,
    dateExpiry datetime NOT NULL,
    dateUpdated timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE KEY id (id),
    UNIQUE KEY clientCode (clientCode)
  ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

CREATE TABLE users (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  username text NOT NULL,
  password text NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES users WRITE;
/*!40000 ALTER TABLE users DISABLE KEYS */;

INSERT INTO users (id, username, password)
VALUES
	(1,'admin','$2a$08$EbmDt349xp8AiMKsjqqM2eBbRkDVQiEe8oucPWKrHHDIyvakHSPn6'),
	(2,'testuser','$2a$08$i/j6vTjadlj8iyxLXU6/IOmWsf6QcUQUsuQ1qkLcGv5ASjT83X96K');

/*!40000 ALTER TABLE users ENABLE KEYS */;
UNLOCK TABLES;