//router.go

package main

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

//NewRouter initialisation
func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc

		router.
			Methods(route.Method).
			Path(os.Getenv("API_VERSION") + route.Pattern).
			Name(route.Name).
			Handler(handler)

	}

	return router
}
