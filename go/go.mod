module bitbucket.org/yenskillah/app-invite-service

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-http-utils/logger v0.0.0-20161128092850-f3a42dcdeae6
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/mux v1.7.3
	github.com/nkanish2002/token_generator v0.0.0-20170724090851-871d6c51c82b
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20190820162420-60c769a6c586
)
