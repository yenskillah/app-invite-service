// main.go

package main

import (
	"os"
)

// @title App Invite Service
// @version 1.0
// @description This an API service that will facilitate the invite token generation and validation for the Catalyst Experience App.
// @contact.email sonnysidramos@gmail.com
// @host 127.0.0.1:8000
// @BasePath /
func main() {
	Initialize(
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_HOST"),
		os.Getenv("APP_DB_PORT"),
		os.Getenv("APP_DB_NAME"),
		os.Getenv("JWT_SIGNING_KEY"))
	Run(os.Getenv("APP_PORT"))
}
