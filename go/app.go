// app.go

package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/go-http-utils/logger"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

var (
	router *mux.Router
	db     *sql.DB
	jwtKey []byte
)

//Initialize the MySQL database connection and the router
func Initialize(user, password, host, port, dbname, signingKey string) {

	connectionString := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, password, host, port, dbname)

	var err error
	db, err = sql.Open("mysql", connectionString)

	if err != nil {
		log.Fatal(err.Error())
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err.Error())
	}

	router = NewRouter()
	jwtKey = []byte(signingKey)
}

//Run the API server
func Run(addr string) {
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"DELETE", "POST", "GET", "PATCH", "OPTIONS"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"X-Requested-With", "Access-Control-Allow-Origin", "Content-Type", "Access-Control-Allow-Headers", "Authorization", "x-admin-access-token"},
	})

	handler := c.Handler(router)
	fmt.Println("Service is up: 127.0.0.1" + addr)
	log.Fatal(http.ListenAndServe(addr, logger.Handler(handler, os.Stdout, logger.CombineLoggerType)))

}
