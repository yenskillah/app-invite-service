//routes.go

package main

import "net/http"

//Route struct for routing settings
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Routes assignments as Route struct
type Routes []Route

var routes = Routes{
	Route{
		"authenticate",
		"POST",
		"/authenticate",
		authenticate,
	},
	Route{
		"getInvites",
		"GET",
		"/invites",
		getInvites,
	},
	Route{
		"createInvite",
		"POST",
		"/invite",
		createInvite,
	},
	Route{
		"getInvite",
		"GET",
		"/invite/{token:[a-zA-Z0-9]+}",
		getInvite,
	},
	Route{
		"updateInvite",
		"PATCH",
		"/invite/{token:[a-zA-Z0-9]+}",
		updateInvite,
	},
	Route{
		"deleteInvite",
		"DELETE",
		"/invite/{token:[a-zA-Z0-9]+}",
		deleteInvite,
	},
}
