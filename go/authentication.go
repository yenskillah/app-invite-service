//authentication.go

package main

import (
	"errors"
	"net/http"

	"github.com/dgrijalva/jwt-go"
)

// Claims for jwt with Username and jwt.StandardClaims as an embedded type
type Claims struct {
	UserID int `json:"userId"`
	jwt.StandardClaims
}

func isAuthorized(r *http.Request) (code int, userID int, err error) {
	if r.Header["X-Admin-Access-Token"] == nil {
		return http.StatusUnauthorized, 0, errors.New("Unauthorized")
	}

	c := &Claims{}
	jwtToken, err := jwt.ParseWithClaims(r.Header["X-Admin-Access-Token"][0], c, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		if jwt.ErrSignatureInvalid.Error() == err.Error() {
			return http.StatusUnauthorized, 0, errors.New("Unauthorized")
		}
		return http.StatusBadRequest, 0, errors.New("Bad request")
	}
	if !jwtToken.Valid {
		return http.StatusUnauthorized, 0, errors.New("Unauthorized")
	}

	return http.StatusOK, c.UserID, nil
}
