//response.go

package main

import (
	"encoding/json"
	"net/http"
)

//ErrorResponse for failed API operations
type ErrorResponse struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
}

//SuccessResponse for successful API operations
type SuccessResponse struct {
	Code   int    `json:"code" example:"200"`
	Result string `json:"message" example:"success"`
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	respondWithJSON(w, code, &ErrorResponse{Code: code, Error: message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
