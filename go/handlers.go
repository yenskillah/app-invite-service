//handlers.go

package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	tokenGenerator "github.com/nkanish2002/token_generator"
	"golang.org/x/crypto/bcrypt"
)

type AccessToken struct {
	Token string `json:"accessToken"`
}

type InviteToken struct {
	Token string `json:"token"`
}

// @Title Request for Admin Access Token
// @Description Request for an Admin Access Token to access the Admin endpoints
// @Accept application/x-www-form-urlencoded
// @Produce json
// @Tags Admin Access Token
// @Param username formData string true "Username"
// @Param password formData string true "Password"
// @Success 200 {object} main.AccessToken
// @Failure 400 {object} main.ErrorResponse "Bad Request"
// @Failure 401 {object} main.ErrorResponse "Unauthorized"
// @Failure 404 {object} main.ErrorResponse "Not Found"
// @Failure 500 {object} main.ErrorResponse "Internal Server Error"
// @Resource /authenticate
// @Router /authenticate [post]
func authenticate(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	username := r.FormValue("username")
	password := r.FormValue("password")

	if username == "" || password == "" {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	c := &Credentials{Username: username}
	if err := c.authenticate(db); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "User Not Found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(c.Password), []byte(password)); err != nil {
		respondWithError(w, http.StatusUnauthorized, "Unauthorized")
		return
	}

	expInMins, _ := strconv.Atoi(os.Getenv("JWT_EXP_TIME_IN_MINS"))
	expirationTime := time.Now().Local().Add(time.Minute * time.Duration(expInMins))
	fmt.Println(expirationTime.Unix())
	claims := &Claims{
		UserID: c.ID,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a := &Authenticate{UserID: c.ID, Token: tokenString}
	if err := a.createAuthentication(db); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	t := &AccessToken{Token: a.Token}
	respondWithJSON(w, http.StatusOK, t)
}

// @Title Generate New Invite Token
// @Description Generate a new app invite token
// @Accept application/x-www-form-urlencoded
// @Produce json
// @Tags Token Invites
// @Param clientCode formData string true "Client Code"
// @Param clientName formData string true "Client Name"
// @Param x-admin-access-token header string true "Admin Access Token"
// @Success 201 {object} main.InviteToken
// @Failure 400 {object} main.ErrorResponse "Bad Request"
// @Failure 401 {object} main.ErrorResponse "Unauthorized"
// @Failure 404 {object} main.ErrorResponse "Not Found"
// @Failure 500 {object} main.ErrorResponse "Internal Server Error"
// @Resource /invite
// @Router /invite [post]
func createInvite(w http.ResponseWriter, r *http.Request) {
	httpStatusCode, userID, err := isAuthorized(r)
	if err != nil {
		respondWithError(w, httpStatusCode, err.Error())
		return
	}

	r.ParseForm()
	clientCode := r.FormValue("clientCode")
	clientName := r.FormValue("clientName")
	if clientCode == "" || clientName == "" {
		respondWithError(w, http.StatusBadRequest, "Bad Request")
		return
	}

	i := &Invite{ClientCode: clientCode, ClientName: clientName}
	tokenGen := tokenGenerator.Generator{}
	tokenGen.New()
	tokenLen, _ := strconv.Atoi(os.Getenv("INVITE_TOKEN_STR_LEN"))
	i.Token = tokenGen.GetToken(tokenLen)
	i.UserID = userID
	if err := i.createInvite(db); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	t := &InviteToken{Token: i.Token}
	respondWithJSON(w, http.StatusCreated, t)
}

// @Title Get Invite's Information
// @Description Get an Invite's Information
// @Accept json
// @Produce json
// @Tags Token Invites
// @Param token path string true "Invite Token"
// @Param x-admin-access-token header string true "Admin Access Token"
// @Success 200 {object} main.Invite;
// @Failure 400 {object} main.ErrorResponse "Bad Request"
// @Failure 401 {object} main.ErrorResponse "Unauthorized"
// @Failure 404 {object} main.ErrorResponse "Not Found"
// @Failure 500 {object} main.ErrorResponse "Internal Server Error"
// @Resource /invite/{token}
// @Router /invite/{token} [get]
func getInvite(w http.ResponseWriter, r *http.Request) {

	httpStatusCode, userID, err := isAuthorized(r)
	if err != nil {
		respondWithError(w, httpStatusCode, err.Error())
		return
	}

	vars := mux.Vars(r)
	token := vars["token"]
	if token == "" {
		respondWithError(w, http.StatusBadRequest, "Invalid Token")
		return
	}

	i := Invite{Token: token, UserID: userID}
	if err := i.getInvite(db); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Token not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, i)
}

// @Title Get All Invites Information
// @Description Get All Invites Information
// @Accept json
// @Produce json
// @Tags Token Invites
// @Param x-admin-access-token header string true "Admin Access Token"
// @Success 200 {array} main.Invite;
// @Failure 400 {object} main.ErrorResponse "Bad Request"
// @Failure 401 {object} main.ErrorResponse "Unauthorized"
// @Failure 404 {object} main.ErrorResponse "Not Found"
// @Failure 500 {object} main.ErrorResponse "Internal Server Error"
// @Resource /invites
// @Router /invites [get]
func getInvites(w http.ResponseWriter, r *http.Request) {

	httpStatusCode, userID, err := isAuthorized(r)
	if err != nil {
		respondWithError(w, httpStatusCode, err.Error())
		return
	}

	i := &Invite{UserID: userID}
	invites, err := i.getInvites(db)
	if err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Token not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	respondWithJSON(w, http.StatusOK, invites)
}

// @Title Validate Invite Token
// @Description Validate an app token
// @Accept json
// @Produce json
// @Tags Token Invites
// @Param token path string true "Invite Token"
// @Success 200 {object} main.SuccessResponse;
// @Failure 400 {object} main.ErrorResponse "Bad Request"
// @Failure 401 {object} main.ErrorResponse "Unauthorized"
// @Failure 404 {object} main.ErrorResponse "Not Found"
// @Failure 500 {object} main.ErrorResponse "Internal Server Error"
// @Resource /invite/{token}
// @Router /invite/{token} [patch]
func updateInvite(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	token := vars["token"]
	if token == "" {
		respondWithError(w, http.StatusBadRequest, "Invalid Token")
		return
	}

	i := &Invite{Token: token}
	if err := i.getInvite(db); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Token not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	if i.Status == "used" {
		respondWithError(w, http.StatusBadRequest, "Invite Token has already been used")
		return
	}

	i.Status = "used"
	layout := "2006-01-02 03:04:05PM MST"
	e, _ := time.Parse(layout, i.DateExpiry+" PST")
	n := time.Now()
	if n.Unix() > e.UTC().Unix() {
		i.Status = "expired"
	}

	if err := i.updateInvite(db); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if i.Status == "expired" {
		respondWithError(w, http.StatusBadRequest, "Invite Token has already expired")
		return
	}

	s := &SuccessResponse{Code: http.StatusOK, Result: "success"}
	respondWithJSON(w, http.StatusOK, s)
}

// @Title Recall Invite Token
// @Description Recall an app token
// @Accept json
// @Produce json
// @Tags Token Invites
// @Param token path string true "Invite Token"
// @Param x-admin-access-token header string true "Admin Access Token"
// @Success 200 {object} main.SuccessResponse;
// @Failure 400 {object} main.ErrorResponse "Bad Request"
// @Failure 401 {object} main.ErrorResponse "Unauthorized"
// @Failure 404 {object} main.ErrorResponse "Not Found"
// @Failure 500 {object} main.ErrorResponse "Internal Server Error"
// @Resource /invite/{token}
// @Router /invite/{token} [delete]
func deleteInvite(w http.ResponseWriter, r *http.Request) {
	httpStatusCode, userID, err := isAuthorized(r)
	if err != nil {
		respondWithError(w, httpStatusCode, err.Error())
		return
	}

	vars := mux.Vars(r)
	token := vars["token"]
	if token == "" {
		respondWithError(w, http.StatusBadRequest, "Invalid token")
		return
	}

	i := Invite{Token: token, UserID: userID}
	if err := i.getInvite(db); err != nil {
		switch err {
		case sql.ErrNoRows:
			respondWithError(w, http.StatusNotFound, "Token not found")
		default:
			respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	i.Status = "inactive"
	if err := i.deleteInvite(db); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	s := &SuccessResponse{Code: http.StatusOK, Result: "success"}
	respondWithJSON(w, http.StatusOK, s)
}
