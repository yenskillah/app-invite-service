//model.go

package main

import (
	"database/sql"
)

type Invite struct {
	ID          int    `json:"id"`
	UserID      int    `json:"userId"`
	ClientCode  string `json:"clientCode"`
	ClientName  string `json:"clientName"`
	Token       string `json:"token"`
	Status      string `json:"status"`
	DateCreated string `json:"dateCreated"`
	DateExpiry  string `json:"dateExpiry"`
}

type Authenticate struct {
	ID     int    `json:"id"`
	UserID int    `json:"userId"`
	Token  string `json:"token"`
}

type Credentials struct {
	ID       int    `json:"-"`
	Username string `json:"username"`
	Password string `json:"password"`
}

func (c *Credentials) authenticate(db *sql.DB) error {
	return db.QueryRow("SELECT id, username, password FROM users WHERE username=?", c.Username).Scan(&c.ID, &c.Username, &c.Password)
}

func (i *Invite) createInvite(db *sql.DB) error {
	stmtIns, err := db.Prepare("INSERT INTO invites (clientCode, clientName, userId, token, status, dateCreated, dateExpiry) VALUES (?, ?, ?, ?, 'active', NOW(), NOW() + INTERVAL 7 DAY) ON DUPLICATE KEY UPDATE token=?, dateCreated=NOW(), dateExpiry=NOW() + INTERVAL 7 DAY")
	if err != nil {
		panic(err.Error())
	}
	defer stmtIns.Close()

	_, err = stmtIns.Exec(i.ClientCode, i.ClientName, i.UserID, i.Token, i.Token)
	if err != nil {
		panic(err.Error())
	}

	return nil
}

func (a *Authenticate) createAuthentication(db *sql.DB) error {
	if err := db.QueryRow("SELECT token FROM  authentication WHERE DATE_ADD(dateCreated,Interval 20 MINUTE) >= NOW() AND userId=? ORDER BY dateCreated DESC LIMIT 1", a.UserID).Scan(&a.Token); err == nil {
		return nil
	}

	stmtIns, err := db.Prepare("INSERT INTO authentication (userId, token, dateCreated) VALUES (?, ?, NOW()) ON DUPLICATE KEY UPDATE token=?, dateCreated=NOW()")
	if err != nil {
		panic(err.Error())
	}
	defer stmtIns.Close()

	_, err = stmtIns.Exec(a.UserID, a.Token, a.Token)
	if err != nil {
		panic(err.Error())
	}

	return nil
}

func (i *Invite) updateInvite(db *sql.DB) error {
	_, err := db.Exec("UPDATE invites SET status=? WHERE token=?", i.Status, i.Token)

	return err
}

func (i *Invite) deleteInvite(db *sql.DB) error {
	_, err := db.Exec("UPDATE invites SET status=? WHERE token=? AND userId=?", i.Status, i.Token, i.UserID)

	return err
}

func (i *Invite) getInvite(db *sql.DB) error {
	if i.UserID == 0 {
		return db.QueryRow("SELECT id, userId, clientCode, clientName, token, status, DATE_FORMAT(dateCreated, '%Y-%m-%d %h:%i:%s%p') as dateExpiry, DATE_FORMAT(dateExpiry, '%Y-%m-%d %h:%i:%s%p') as dateExpiry FROM invites WHERE token=?", i.Token).Scan(&i.ID, &i.UserID, &i.ClientCode, &i.ClientName, &i.Token, &i.Status, &i.DateCreated, &i.DateExpiry)
	}

	return db.QueryRow("SELECT id, userId, clientCode, clientName, token, status, DATE_FORMAT(dateCreated, '%Y-%m-%d %h:%i:%s%p') as dateExpiry, DATE_FORMAT(dateExpiry, '%Y-%m-%d %h:%i:%s%p') as dateExpiry FROM invites WHERE token=? AND userId=?", i.Token, i.UserID).Scan(&i.ID, &i.UserID, &i.ClientCode, &i.ClientName, &i.Token, &i.Status, &i.DateCreated, &i.DateExpiry)
}

func (i *Invite) getInvites(db *sql.DB) ([]Invite, error) {
	rows, err := db.Query("SELECT id, clientCode, clientName, token, status, dateCreated, dateExpiry FROM invites WHERE userId=?", i.UserID)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	invites := []Invite{}

	for rows.Next() {
		var i Invite
		if err := rows.Scan(&i.ID, &i.ClientCode, &i.ClientName, &i.Token, &i.Status, &i.DateCreated, &i.DateExpiry); err != nil {
			return nil, err
		}
		invites = append(invites, i)
	}

	return invites, nil
}
