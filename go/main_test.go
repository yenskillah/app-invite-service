// main_test.go
package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strconv"
	"testing"
)

var (
	accessToken string
	inviteToken string
)

func TestMain(m *testing.M) {
	Initialize(
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_HOST"),
		os.Getenv("APP_DB_PORT"),
		os.Getenv("APP_DB_NAME"),
		os.Getenv("JWT_SIGNING_KEY"))
	ensureTableExists()
	code := m.Run()
	purgeTable()
	os.Exit(code)
}

func ensureTableExists() {
	if _, err := db.Exec(tableAuthenticationCreationQuery); err != nil {
		log.Fatal(err)
	}
	if _, err := db.Exec(tableInvitesCreationQuery); err != nil {
		log.Fatal(err)
	}
}
func purgeTable() {
	db.Exec("DELETE FROM authentication")
	db.Exec("ALTER TABLE authentication AUTO_INCREMENT = 1")

	db.Exec("DELETE FROM invites")
	db.Exec("ALTER TABLE invites AUTO_INCREMENT = 1")
}

const tableAuthenticationCreationQuery = `CREATE TABLE IF NOT EXISTS authentication (
    id int(11) unsigned NOT NULL AUTO_INCREMENT,
    userId int(11) DEFAULT NULL,
    token text,
    dateCreated timestamp NULL DEFAULT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY userId (userId)
  ) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;`

const tableInvitesCreationQuery = `  
  CREATE TABLE IF NOT EXISTS invites (
    id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    userId bigint(20) NOT NULL,
    token text NOT NULL,
    clientCode varchar(12) NOT NULL DEFAULT '',
    clientName text NOT NULL,
    status enum('active','inactive','used','expired') NOT NULL DEFAULT 'active',
    dateCreated datetime NOT NULL,
    dateExpiry datetime NOT NULL,
    dateUpdated timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE KEY id (id),
    UNIQUE KEY clientCode (clientCode)
  ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;`

func TestPostAuthenticate(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	validateResponseCode(t, http.StatusOK, response.Code)
}

func TestPostFailedAuthenticate(t *testing.T) {
	purgeTable()

	response := getInvalidAccessToken()
	validateResponseCode(t, http.StatusNotFound, response.Code)
}

func TestPostCreateInvite(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	response = generateTokenInvite("", a.Token)
	validateResponseCode(t, http.StatusCreated, response.Code)
}

func TestPostFailedCreateInvite(t *testing.T) {
	purgeTable()

	response := getInvalidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}
	response = generateTokenInvite("", a.Token)
	validateResponseCode(t, http.StatusBadRequest, response.Code)
}

func TestGetInvite(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	response = generateTokenInvite("", a.Token)
	decoder = json.NewDecoder(response.Body)
	var i Invite
	err = decoder.Decode(&i)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("GET", os.Getenv("API_VERSION")+"/invite/"+i.Token, nil)
	req.Header.Set("X-Admin-Access-Token", a.Token)
	response = runRequest(req)
	validateResponseCode(t, http.StatusOK, response.Code)
}

func TestGetNonExistentInvite(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	validateResponseCode(t, http.StatusOK, response.Code)
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("GET", os.Getenv("API_VERSION")+"/invite/xxx", nil)
	req.Header.Set("X-Admin-Access-Token", a.Token)
	response = runRequest(req)
	validateResponseCode(t, http.StatusNotFound, response.Code)

}

func TestDeleteInvite(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	response = generateTokenInvite("", a.Token)
	decoder = json.NewDecoder(response.Body)
	var i Invite
	err = decoder.Decode(&i)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("DELETE", os.Getenv("API_VERSION")+"/invite/"+i.Token, nil)
	req.Header.Set("X-Admin-Access-Token", a.Token)
	response = runRequest(req)
	validateResponseCode(t, http.StatusOK, response.Code)
}

func TestDeleteNonExistentInvite(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("DELETE", os.Getenv("API_VERSION")+"/invite/xxx", nil)
	req.Header.Set("X-Admin-Access-Token", a.Token)
	response = runRequest(req)
	validateResponseCode(t, http.StatusNotFound, response.Code)
}

func TestGetInvites(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	for i := 1; i < 5; i++ {
		response = generateTokenInvite(strconv.Itoa(i), a.Token)
		decoder = json.NewDecoder(response.Body)
		var i Invite
		err = decoder.Decode(&i)
		if err != nil {
			panic(err)
		}
	}

	req, _ := http.NewRequest("GET", os.Getenv("API_VERSION")+"/invites", nil)
	req.Header.Set("X-Admin-Access-Token", a.Token)
	response = runRequest(req)
	validateResponseCode(t, http.StatusOK, response.Code)
}

func TestGetNonExistentInvites(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("GET", os.Getenv("API_VERSION")+"/invites", nil)
	req.Header.Set("X-Admin-Access-Token", a.Token)
	response = runRequest(req)
	validateResponseCode(t, http.StatusOK, response.Code)
}

func TestUpdateInvite(t *testing.T) {
	purgeTable()

	response := getValidAccessToken()
	decoder := json.NewDecoder(response.Body)
	var a AccessToken
	err := decoder.Decode(&a)
	if err != nil {
		panic(err)
	}

	response = generateTokenInvite("", a.Token)
	decoder = json.NewDecoder(response.Body)
	var i Invite
	err = decoder.Decode(&i)
	if err != nil {
		panic(err)
	}

	req, _ := http.NewRequest("PATCH", os.Getenv("API_VERSION")+"/invite/"+i.Token, nil)
	response = runRequest(req)
	validateResponseCode(t, http.StatusOK, response.Code)
}

func TestUpdateNonExistentInvite(t *testing.T) {
	purgeTable()

	req, _ := http.NewRequest("PATCH", os.Getenv("API_VERSION")+"/invite/xxx", nil)
	response := runRequest(req)
	validateResponseCode(t, http.StatusNotFound, response.Code)
}

func runRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()

	router.ServeHTTP(rr, req)

	return rr
}

func validateResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func generateTokenInvite(x string, token string) *httptest.ResponseRecorder {
	data := url.Values{}
	data.Add("clientCode", "CLNT"+x)
	data.Add("clientName", "Client Telecoms Pte Ltd. "+x)

	req, _ := http.NewRequest("POST", os.Getenv("API_VERSION")+"/invite", bytes.NewBufferString(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	req.Header.Set("X-Admin-Access-Token", token)

	return runRequest(req)
}

func getValidAccessToken() *httptest.ResponseRecorder {
	data := url.Values{}
	data.Add("username", "testuser")
	data.Add("password", "testuser1q2w3e")

	req, _ := http.NewRequest("POST", os.Getenv("API_VERSION")+"/authenticate", bytes.NewBufferString(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")

	return runRequest(req)
}

func getInvalidAccessToken() *httptest.ResponseRecorder {
	data := url.Values{}
	data.Add("username", "testuserx")
	data.Add("password", "testuser1q2w3ex")

	req, _ := http.NewRequest("POST", os.Getenv("API_VERSION")+"/authenticate", bytes.NewBufferString(data.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")

	return runRequest(req)
}
