//logger.go

package main

import (
	"log"
	"net/http"
	"time"
)

//Logger prints the access logs for each API endpoints
func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s\t%s\t%s",
			r.Host,
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
			r.Header,
		)
	})
}
